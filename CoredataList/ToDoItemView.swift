//
//  ToDoItemView.swift
//  CoredataList
//
//  Created by Nouman Saleem on 17/05/2021.
//  Copyright © 2021 Nouman Saleem. All rights reserved.
//

import SwiftUI

struct ToDoItemView: View {
 
    var title:String = ""
    var createdAt:String = ""
    
    var body: some View{
        HStack{
            VStack(alignment: .leading){
                Text(title)
                    .font(.headline)
                Text(createdAt)
                    .font(.caption)
            }
        }
        
    }

struct ToDoItemView_Previews: PreviewProvider {
    static var previews: some View {
        ToDoItemView()
    }
}
}
