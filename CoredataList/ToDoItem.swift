//
//  ToDoItem.swift
//  CoredataList
//
//  Created by Nouman Saleem on 17/05/2021.
//  Copyright © 2021 Nouman Saleem. All rights reserved.
//

import Foundation
import CoreData

public class ToDoItem: NSManagedObject, Identifiable {
    @NSManaged public var createdAt:Date?
    @NSManaged public var title:String?
    
}

extension ToDoItem {
    
    static func GetAllToDoItems() -> NSFetchRequest<ToDoItem> {
        let request:NSFetchRequest<ToDoItem> = ToDoItem.fetchRequest() as! NSFetchRequest<ToDoItem>
        
        let sortDescriptor = NSSortDescriptor(key: "createdAt", ascending: true)
        request.sortDescriptors = [sortDescriptor]
        
        return request  
    }
}
    
    

