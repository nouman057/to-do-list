//
//  ContentView.swift
//  CoredataList
//
//  Created by Nouman Saleem on 17/05/2021.
//  Copyright © 2021 Nouman Saleem. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    
    @Environment(\.managedObjectContext) var managedObjectContext
    @FetchRequest(fetchRequest: ToDoItem.GetAllToDoItems()) var toDoItem:FetchedResults<ToDoItem>
    
    @State private var newToDoItem = ""
    
    var body: some View {
        NavigationView{
            List{
                Section(header: Text("What's Next")){
                    HStack{
                        TextField("New Item", text: self.$newToDoItem)
                        Button(action: {
                            let toDoItem = ToDoItem(context: self.managedObjectContext)
                            toDoItem.title = self.newToDoItem
                            toDoItem.createdAt = Date()
                            
                            do{
                                try self.managedObjectContext.save()
                            }
                            catch{
                                print(error)
                            }
                            
                            self.newToDoItem = ""
                            
                            
                        }) {
                            Image(systemName: "plus.circle.fill")
                                .foregroundColor(.green)
                                .imageScale(.large)
                        }
                        
                    }
                }.font(.headline)
                
                Section(header: Text("To Do's")) {
                    ForEach(self.toDoItem) {toDoItem in
                        ToDoItemView(title: toDoItem.title!	 , createdAt: "\(toDoItem.createdAt!)")
                    }.onDelete {indexSet in

                        let deleteItem = self.toDoItem[indexSet.first!]
                        self.managedObjectContext.delete(deleteItem)
                        
                        do{
                            try self.managedObjectContext.save()
                        }
                        catch{
                            print(error)
                        }
                        
                    }
                }
                
            }
            .navigationBarTitle(Text("My List"))
            .navigationBarItems(trailing: EditButton())
        }
        
        
    }

}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
